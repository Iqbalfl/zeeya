<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use App\User;
use Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
    	$validator = Validator::make($request->all(), [
			'name' => 'required|string|max:255',
			'email' => 'required|email|unique:users',
			'gender' => 'required|string',
			'phone' => 'required|numeric',
			'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->passes()) {
        	
        	$user = new User;
	        $user->name = $request->name;
	        $user->email = $request->email;
	        $user->gender = $request->gender;
	        $user->phone = $request->phone;
	        $user->password = Hash::make($request->password);
	        $user->save();

	        //$user->attachRole('user');

	        $token = Str::random(60);
	        $user->forceFill([
	            'api_token' => hash('sha256', $token),
	        ])->save();
        
        	$result['data'] = $user;
        	$result['status'] = true;
            $result['message'] = "Registrasi berhasil!";
        }
        else
        {
        	$result['status'] = false;
            $result['message'] = "Gagal terjadi kesalahan!";
            $result['error'] = $validator->errors();
        	$result['data'] = [];
        }
        
        $result['code'] = 200;

        return response()->json($result);
    }

    public function login(Request $request)
    {
    	$validator = \Validator::make($request->all(), [
    			'email' => 'required|string|max:50',
                'password' => 'required|string',
    	]);

    	if ($validator->passes()) {

    		$email = $request->email;
	        $password = $request->password;

	        $user = User::where('email',$email)->first();
	        
	        if($user){ 
	            if(Hash::check($password,$user->password)){

	            	if ($user->api_token == null) {
				        $token = Str::random(60);
				        $user->forceFill([
				            'api_token' => hash('sha256', $token),
				        ])->save();
			        }

	            	$result['data']= $user;
					$result['status'] = true;
				    $result['message'] = "Login Sukses";
	               
	            }
	            else{
	               $result['status'] = false;
				   $result['message'] = "Email dan Password tidak cocok!";
	            }
	        }
	        else{
	            $result['status'] = false;
				$result['message'] = "Email belum terdaftar!";
	        }
    	}
    	else
    	{
    		$result['status'] = false;
    	    $result['message'] = "Failed";
    	    $result['error'] = $validator->errors();
    		$result['data'] = null;
    	}
    	
    	$result['code'] = 200;

    	return response()->json($result);
    }

    public function profile(Request $request)
    {
    	$user = \Auth::user();

    	if ($user->avatar != null) {
    		$user->avatar = url('uploads/images/avatars/'.$user->avatar);
    	}

        $result['data'] = $user;
        $result['status'] = true;
        $result['message'] = "Success";
        $result['code'] = 200;

        return response()->json($result);
    }

    public function profileUpdate(Request $request)
    {    	 
    	$user_id = \Auth::user()->id;

        $validator = \Validator::make($request->all(), [
          'name' => 'required|string|max:255',
          'email' => 'required|unique:users,email,' . $user_id,
          'gender' => 'required|string',
          'phone' => 'required|numeric',
        ]);

        if ($validator->passes()) {
	        $user = User::find($user_id);
	        $user->name = $request->name;
	        $user->email = $request->email;
	        $user->gender = $request->gender;
	        $user->phone = $request->phone;

	        if ($request->hasFile('avatar')) {
	            // megnambil image yang diupload berikut ekstensinya
	            $filename = null;
	            $uploaded_image = $request->file('avatar');
	            $extension = $uploaded_image->getClientOriginalExtension();
	            // membuat nama file random dengan extension
	            $filename = md5(time()) . '.' . $extension;
	            
	            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars';

	            // resize photo and save
		        Image::make($request->file('avatar'))
		            ->resize(500, 500, function ($constraints) {
		                $constraints->aspectRatio();
		            })
		            ->save($destinationPath . DIRECTORY_SEPARATOR . $filename);

	            // hapus image lama, jika ada
	            if ($user->avatar) {
	                $old_image = $user->avatar;
	                if ($user->avatar != 'user-default.jpg') {
	                    # delete if not default
	                    $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars'
	                     . DIRECTORY_SEPARATOR . $user->avatar;
	                    try {
	                        File::delete($filepath);
	                    } catch (FileNotFoundException $e) {
	                        // File sudah dihapus/tidak ada
	                    }
	                }
	            }
	            // ganti field image dengan image yang baru
	            $user->avatar = $filename;
	        }

	        $user->save();
	        
        	$result['data'] = $user;
        	$result['status'] = true;
            $result['message'] = "Profile berhasil diupdate";
        }
        else
        {
        	$result['status'] = false;
            $result['message'] = "Gagal terjadi kesalahan!";
            $result['error'] = $validator->errors();
        	$result['data'] = [];
        }
        
        $result['code'] = 200;

        return response()->json($result);
    }

    public function profileUpdateAvatar(Request $request)
    {    	 
    	$user_id = \Auth::user()->id;

        $validator = \Validator::make($request->all(), [
          'avatar' => 'required|image',
        ]);

        if ($validator->passes()) {

	        $user = User::find($user_id);

	        if ($request->hasFile('avatar')) {
	            // megnambil image yang diupload berikut ekstensinya
	            $filename = null;
	            $uploaded_image = $request->file('avatar');
	            $extension = $uploaded_image->getClientOriginalExtension();
	            // membuat nama file random dengan extension
	            $filename = md5(time()) . '.' . $extension;
	            
	            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars';

	            // resize photo and save
		        Image::make($request->file('avatar'))
		            ->resize(500, 500, function ($constraints) {
		                $constraints->aspectRatio();
		            })
		            ->save($destinationPath . DIRECTORY_SEPARATOR . $filename);

	            // hapus image lama, jika ada
	            if ($user->avatar) {
	                $old_image = $user->avatar;
	                if ($user->avatar != 'user-default.jpg') {
	                    # delete if not default
	                    $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars'
	                     . DIRECTORY_SEPARATOR . $user->avatar;
	                    try {
	                        File::delete($filepath);
	                    } catch (FileNotFoundException $e) {
	                        // File sudah dihapus/tidak ada
	                    }
	                }
	            }
	            // ganti field image dengan image yang baru
	            $user->avatar = $filename;
	        }

	        $user->save();
	        
        	$result['data'] = $user;
        	$result['status'] = true;
            $result['message'] = "Avatar berhasil diupdate";
        }
        else
        {
        	$result['status'] = false;
            $result['message'] = "Gagal terjadi kesalahan!";
            $result['error'] = $validator->errors();
        	$result['data'] = [];
        }
        
        $result['code'] = 200;

        return response()->json($result);
    }
}
