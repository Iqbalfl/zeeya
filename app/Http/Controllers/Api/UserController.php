<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use App\User;
use Validator;

class UserController extends Controller
{
    public function index(Request $request)
    {
    	$users = User::query();

    	if ($request->has('sort')){
	        $sorts = explode(',', $request->sort);
	        foreach ($sorts as $sort) {
				$field = preg_replace('/[-]/', '', $sort);
				if (preg_match('/^[-]/', $sort)) {
					$users->orderBy($field, 'desc');
				} else {
					$users->orderBy($field, 'asc');
				}
	        }
	    }

	    if ($request->has('name')) {
	        $users->where('name','LIKE','%'.$request->name.'%');
	    }

	    $users = $users->paginate($request->input('offset', 10))->appends($request->all());

	    foreach ($users as $user) {
	    	if ($user->avatar != null) {
	    		$user->avatar = url('uploads/images/avatars/'.$user->avatar);
	    	}
	    }

        //$result['data'] = $users;

        if ($users instanceof \Illuminate\Pagination\LengthAwarePaginator) {
    		$result['pagination']['total_records'] = $users->total();
            $result['pagination']['offset'] = $users->perPage();
            $result['pagination']['current_page'] =$users->currentpage();
            $result['pagination']['last_page']=$users->lastPage();
            $result['pagination']['next']=$users->nextPageUrl();
            $result['pagination']['prev']=$users->previousPageUrl();
    		$result['data'] = $users->all();
    	} else {
    		$result['data'] = $users;
    	}

        $result['status'] = true;
        $result['message'] = "Success";
        $result['code'] = 200;

        return response()->json($result);
    }

    public function show(Request $request, $id)
    {
    	$user = User::find($id);

    	if ($user) {
	    	if ($user->avatar != null) {
	    		$user->avatar = url('uploads/images/avatars/'.$user->avatar);
	    	}
    	}

        $result['data'] = $user;
        $result['status'] = true;
        $result['message'] = "Success";
        $result['code'] = 200;

        return response()->json($result);
    }

    public function update(Request $request, $user_id)
    {
        $validator = \Validator::make($request->all(), [
          'name' => 'required|string|max:255',
          'email' => 'required|unique:users,email,' . $user_id,
          'gender' => 'required|string',
          'phone' => 'required|numeric',
        ]);

        if ($validator->passes()) {
            $user = User::find($user_id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->gender = $request->gender;
            $user->phone = $request->phone;

            if ($request->hasFile('avatar')) {
                // megnambil image yang diupload berikut ekstensinya
                $filename = null;
                $uploaded_image = $request->file('avatar');
                $extension = $uploaded_image->getClientOriginalExtension();
                // membuat nama file random dengan extension
                $filename = md5(time()) . '.' . $extension;
                
                $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars';

                // resize photo and save
                Image::make($request->file('avatar'))
                    ->resize(500, 500, function ($constraints) {
                        $constraints->aspectRatio();
                    })
                    ->save($destinationPath . DIRECTORY_SEPARATOR . $filename);

                // hapus image lama, jika ada
                if ($user->avatar) {
                    $old_image = $user->avatar;
                    if ($user->avatar != 'user-default.jpg') {
                        # delete if not default
                        $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars'
                         . DIRECTORY_SEPARATOR . $user->avatar;
                        try {
                            File::delete($filepath);
                        } catch (FileNotFoundException $e) {
                            // File sudah dihapus/tidak ada
                        }
                    }
                }
                // ganti field image dengan image yang baru
                $user->avatar = $filename;
            }

            $user->save();
            
            $result['data'] = $user;
            $result['status'] = true;
            $result['message'] = "User berhasil diupdate";
        }
        else
        {
            $result['status'] = false;
            $result['message'] = "Gagal terjadi kesalahan!";
            $result['error'] = $validator->errors();
            $result['data'] = [];
        }
        
        $result['code'] = 200;

        return response()->json($result);
    }

    public function destroy($id)
    {
        $user = User::find($id);

        if ($user->photo) {
            $old_image = $user->photo;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars'
            . DIRECTORY_SEPARATOR . $user->photo;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }

        $user->delete();

        $result['data'] = [];
        $result['status'] = true;
        $result['message'] = "User berhasil dihapus";

        $result['code'] = 200;

        return response()->json($result);
    }
}
