<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use App\Product;
use Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::query();

        if ($request->has('sort')){
            $sorts = explode(',', $request->sort);
            foreach ($sorts as $sort) {
                $field = preg_replace('/[-]/', '', $sort);
                if (preg_match('/^[-]/', $sort)) {
                    $products->orderBy($field, 'desc');
                } else {
                    $products->orderBy($field, 'asc');
                }
            }
        }

        if ($request->has('name')) {
            $products->where('name','LIKE','%'.$request->name.'%');
        }

        $products = $products->paginate($request->input('offset', 10))->appends($request->all());

        foreach ($products as $product) {
            if ($product->photo != null) {
                $product->photo = url('uploads/images/products/'.$product->photo);
            }
        }

        //$result['data'] = $products;

        if ($products instanceof \Illuminate\Pagination\LengthAwarePaginator) {
            $result['pagination']['total_records'] = $products->total();
            $result['pagination']['offset'] = $products->perPage();
            $result['pagination']['current_page'] =$products->currentpage();
            $result['pagination']['last_page']=$products->lastPage();
            $result['pagination']['next']=$products->nextPageUrl();
            $result['pagination']['prev']=$products->previousPageUrl();
            $result['data'] = $products->all();
        } else {
            $result['data'] = $products;
        }

        $result['status'] = true;
        $result['message'] = "Success";
        $result['code'] = 200;

        return response()->json($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
          'name' => 'required|string',
          'description' => 'required|string',
          'stock' => 'required|numeric',
          'price' => 'required|numeric',
          'photo' => 'image|max:2048',
        ]);

        if ($validator->passes()) {
            
            $product = Product::create($request->except('photo'));

            if ($request->hasFile('photo')) {
                // mengambil image yang diupload berikut ekstensinya
                $filename = null;
                $uploaded_image = $request->file('photo');
                $extension = $uploaded_image->getClientOriginalExtension();
                // membuat nama file random dengan extension
                $filename = md5(time()) . '.' . $extension;
                
                $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'products';

                // resize photo and save
                Image::make($request->file('photo'))
                    ->resize(500, 500, function ($constraints) {
                        $constraints->aspectRatio();
                    })
                    ->save($destinationPath . DIRECTORY_SEPARATOR . $filename);

                // ganti field image dengan image yang baru
                $product->photo = $filename;
            }

            $product->save();
            
            $result['data'] = $product;
            $result['status'] = true;
            $result['message'] = "Product berhasil dibuat";
        }
        else
        {
            $result['status'] = false;
            $result['message'] = "Gagal terjadi kesalahan!";
            $result['error'] = $validator->errors();
            $result['data'] = [];
        }
        
        $result['code'] = 200;

        return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if ($product) {
            if ($product->photo != null) {
                $product->photo = url('uploads/images/products/'.$product->photo);
            }
        }

        $result['data'] = $product;
        $result['status'] = true;
        $result['message'] = "Success";
        $result['code'] = 200;

        return response()->json($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
          'name' => 'required|string',
          'description' => 'required|string',
          'stock' => 'required|numeric',
          'price' => 'required|numeric',
          'photo' => 'image|max:2048',
        ]);

        if ($validator->passes()) {
            
            $product = Product::find($id);
            $product->name = $request->name;
            $product->description = $request->description;
            $product->stock = $request->stock;
            $product->price = $request->price;

            if ($request->hasFile('photo')) {
                // mengambil image yang diupload berikut ekstensinya
                $filename = null;
                $uploaded_image = $request->file('photo');
                $extension = $uploaded_image->getClientOriginalExtension();
                // membuat nama file random dengan extension
                $filename = md5(time()) . '.' . $extension;
                
                $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'products';

                // resize photo and save
                Image::make($request->file('photo'))
                    ->resize(500, 500, function ($constraints) {
                        $constraints->aspectRatio();
                    })
                    ->save($destinationPath . DIRECTORY_SEPARATOR . $filename);

                // hapus image lama, jika ada
                if ($product->photo) {
                    $old_image = $product->photo;
                    $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'products'
                     . DIRECTORY_SEPARATOR . $product->photo;
                    try {
                        File::delete($filepath);
                    } catch (FileNotFoundException $e) {
                        // File sudah dihapus/tidak ada
                    }
                }

                // ganti field image dengan image yang baru
                $product->photo = $filename;
            }

            $product->save();
            
            $result['data'] = $product;
            $result['status'] = true;
            $result['message'] = "Product berhasil diedit";
        }
        else
        {
            $result['status'] = false;
            $result['message'] = "Gagal terjadi kesalahan!";
            $result['error'] = $validator->errors();
            $result['data'] = [];
        }
        
        $result['code'] = 200;

        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if ($product->photo) {
            $old_image = $product->photo;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'products'
            . DIRECTORY_SEPARATOR . $product->photo;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }

        $product->delete();

        $result['data'] = [];
        $result['status'] = true;
        $result['message'] = "Product berhasil dihapus";

        $result['code'] = 200;

        return response()->json($result);
    }
}
