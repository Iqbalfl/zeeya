<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles Bootstrap 4 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Jquery, Popper and Bootstrap Js -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"></script>

  </head>
  <body>
    <div id="app">
      <nav class="navbar navbar-expand-md navbar-light bg-light">
        <div class="container">
          <a class="navbar-brand" href="{{ url('/') }}">
            Demo App
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
              <!-- Authentication Links -->
              @guest
              <li class="nav-item">
                <a class="nav-link" href="#">{{ __('Login') }}</a>
              </li>
              @if (Route::has('register'))
              <li class="nav-item">
                <a class="nav-link" href="#">{{ __('Register') }}</a>
              </li>
              @endif
              @else
              <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
                  </a>
                  <form id="logout-form" action="#" method="POST" style="display: none;">
                    @csrf
                  </form>
                </div>
              </li>
              @endguest
            </ul>
          </div>
        </div>
      </nav>
      <main class="py-4">
        <div class="row">
          <div class="col-md-10 offset-md-1">

            @if (Session::has('flash_notification.message'))
              <div class="alert alert-{{ Session::get('flash_notification.level') }} alert-dismissible fade show" role="alert">
                <strong>{{ Session::get('flash_notification.title') }}</strong> {{ Session::get('flash_notification.message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            @endif

            <div class="table-responsive">
              <table class="table table-striped table-bordered">
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Gender</th>
                  <th>Telepon</th>
                  <th>Aksi</th>
                </tr>
                @foreach ($users as $key => $item)
                  <tr>
                    <td>{{ $users->firstItem() + $key }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->gender ?? '-' }}</td>
                    <td>{{ $item->phone ?? '-' }}</td>
                    <td>
                      <button onclick="showModal('{{ $item->id }}');" type="button" class="btn btn-warning btn-icon btn-sm"><i class="fa fa-edit"></i></button>
                    </td>
                  </tr>
                @endforeach
              </table>
              {{ $users->links() }}
            </div>
          </div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="edit-modal" role="dialog" aria-labelledby="edit-modal" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form  method="post" id="form-edit" action="#" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')

                  <input type="hidden" id="user_id" value="">

                  <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" id="name" class="form-control" name="name">
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="email" class="form-control" name="email">
                  </div>

                  <div class="form-group">
                    <label for="gender">Gender</label>
                    <input type="gender" id="gender" class="form-control" name="gender">
                  </div>

                  <div class="form-group">
                    <label for="phone">Nomor Telephone</label>
                    <input type="text" id="phone" class="form-control" name="phone">
                  </div>

                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="submitForm()"><i class="fa fa-redo"></i> Save</button>
              </div>
            </div>
          </div>
        </div>

        <script type="text/javascript">
          
          function showModal(user_id) {
            $('#edit-modal').modal('show');

            if(user_id) {
              $.ajax({
                url: '{{url('/demo/user')}}'+'/'+user_id+'/edit',
                type: "GET",
                dataType: "json",
                success:function(data)
                {
                  // set data from ajax to input modal
                  $('#user_id').val(data.id);
                  $('#name').val(data.name);
                  $('#email').val(data.email);
                  $('#gender').val(data.gender);
                  $('#phone').val(data.phone);

                  // show modal
                  $('#edit-modal').modal('show');
                }
              });
            } 
          }

          function submitForm() {

            var user_id = $('#user_id').val();

            if (user_id) {
              $('#form-edit').attr('action', '{{url('/demo/user')}}'+'/'+user_id).submit();
            } else {
              alert('Tidak ada data untuk di update!');
            }
          }

        </script>

      </main>
    </div>
  </body>
</html>