<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        foreach (range(1, 25) as $val) {
    	  	
    	  	// insert data ke table pegawai menggunakan Faker
    		User::create([
    			'name' => $faker->name($gender = 'male'),
    			'email' => $faker->unique()->safeEmail,
    			'gender' => 'Laki-laki',
    			'phone' => $faker->phoneNumber,
    			'password' => '$10$tQ9ZFyAoqHgQU5W/Coq/A.cu5NWTm/T9VCQ.4PK.NtxfXZxFSGT/S',
    		]);
        }
    }
}
