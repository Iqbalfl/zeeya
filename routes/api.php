<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['prefix'=>'/v1/user'], function() {
    
	Route::post('/register', 'Api\AuthController@register');
	Route::post('/login', 'Api\AuthController@login');
	Route::get('/profile', 'Api\AuthController@profile')->middleware('auth:api');
	Route::post('/profile', 'Api\AuthController@profileUpdate')->middleware('auth:api');
	Route::post('/profile/avatar', 'Api\AuthController@profileUpdateAvatar')->middleware('auth:api');
	
	Route::get('/', 'Api\UserController@index')->middleware('auth:api');
	Route::get('/{id}', 'Api\UserController@show')->middleware('auth:api');
	Route::put('/{id}', 'Api\UserController@update')->middleware('auth:api');
	Route::delete('/{id}', 'Api\UserController@destroy')->middleware('auth:api');

});

Route::group(['prefix'=>'/v1/product'], function() {
	Route::get('/', 'Api\ProductController@index')->middleware('auth:api');
	Route::post('/', 'Api\ProductController@store')->middleware('auth:api');
	Route::get('/{id}', 'Api\ProductController@show')->middleware('auth:api');
	Route::put('/{id}', 'Api\ProductController@update')->middleware('auth:api');
	Route::delete('/{id}', 'Api\ProductController@destroy')->middleware('auth:api');
});